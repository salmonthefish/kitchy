package com.gsalmon.kitchy

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KitchyApplication

fun main(args: Array<String>) {
  runApplication<KitchyApplication>(*args)
}
